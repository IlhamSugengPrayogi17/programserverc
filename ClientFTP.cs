﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

class Program
{
    static StreamWriter writeData;
    static NetworkStream nStream;
    static string BaseData;
    static string BlockData;
    static int RemainingStringLength = 0;
    static bool Done = false;

    static void Main(string[] args)
    {
        try
        {

            TcpClient tcpClient = new TcpClient("127.0.0.1", 5555);
            nStream = tcpClient.GetStream();
            writeData = new StreamWriter(nStream);

            FileStream fs = File.OpenRead("nama File");
            byte[] Data = new byte[fs.Length];
            fs.Read(Data, 0, Data.Length);

            BaseData = Convert.ToBase64String(Data);

            int startIndex = 0;

            Console.WriteLine("Transfering...");

            while (Done == false)
            {
                while (startIndex < BaseData.Length)
                {
                    try
                    {
                        BlockData = BaseData.Substring(startIndex, 100);
                        writeData.WriteLine(BlockData);
                        writeData.Flush();
                        startIndex += 100;
                    }
                    catch (Exception er)
                    {
                        RemainingStringLength = BaseData.Length - startIndex;
                        BlockData = BaseData.Substring(startIndex, RemainingStringLength);
                        writeData.WriteLine(BlockData);
                        writeData.Flush();
                        Done = true;
                        break;
                    }
                }
            }
            writeData.Close();
            tcpClient.Close();

            Console.WriteLine("Transfer Complete");
        }
        catch (Exception er)
        {
            Console.WriteLine("Unable to connect to server");
            Console.WriteLine(er.Message);
        }
        Console.ReadKey();
    }
}
